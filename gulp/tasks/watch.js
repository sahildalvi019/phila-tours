var gulp = require("gulp"),
    watch = require("gulp-watch"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssvars = require("postcss-simple-vars"),
    nested = require("postcss-nested"),
    cssImport = require("postcss-import"),
    browserSync = require("browser-sync").create();

gulp.task("watch", function () {
    browserSync.init({
        notify: false,
        server: {
            baseDir: "app"
        }
    });
    watch("./app/index.html", function () {
        browserSync.reload();
    });

    watch("./app/assets/styles/**/*.css", gulp.series("css", "cssInject"));
});

gulp.task("cssInject", function () {
    return gulp.src("./app/css/style.css")
        .pipe(browserSync.stream());
});
